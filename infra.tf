variable "project" {}
variable "region" {}

provider "google" {
  region  = "${var.region}"
  project = "${var.project}"
  region  = "${var.region}"
}

terraform {
  backend "gcs" {
    bucket = "karomoomoo_tf_state"
    prefix = "terraform/state"
  }
}

# This cluster is somewhat weird (has a default node pool) because I created it manually in
# September 2018 and only now imported it to Terraform.
resource "google_container_cluster" "moocluster" {
  name     = "moo"
  location = "europe-west1-d"

  initial_node_count = 0

  # Setting an empty username and password explicitly disables basic auth.
  master_auth {
    username = ""
    password = ""
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
    ]
  }
}

resource "google_container_node_pool" "moocluster-default-pool" {
  name       = "default-pool"
  location = "europe-west1-d"
  cluster    = "${google_container_cluster.moocluster.name}"
  node_count = 2

  node_config {
    machine_type = "n1-standard-1"
  }
}


resource "google_service_account" "gcr-writer" {
  account_id   = "gcr-writer"
  display_name = "Service account to upload Docker images to GCR"
}

resource "google_storage_bucket_iam_member" "gcr-writer-can-admin-images" {
  bucket = "eu.artifacts.karomoomoo.appspot.com"
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.gcr-writer.email}"
}

resource "google_service_account_key" "gcr-key" {
  service_account_id = "${google_service_account.gcr-writer.name}"
  public_key_type = "TYPE_X509_PEM_FILE"
}

resource "google_service_account" "moocluster-developer" {
  account_id   = "moocluster-developer"
  display_name = "Service account to deploy to GCR"
}

resource "google_project_iam_member" "project" {
  role    = "roles/container.developer"
  member = "serviceAccount:${google_service_account.moocluster-developer.email}"
}

resource "google_service_account_key" "gke-key" {
  service_account_id = "${google_service_account.moocluster-developer.name}"
  public_key_type = "TYPE_X509_PEM_FILE"
}

output "client_certificate" {
  value = "${google_container_cluster.moocluster.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.moocluster.master_auth.0.client_key}"
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.moocluster.master_auth.0.cluster_ca_certificate}"
}

output "cluster_ip" {
  value = "${google_container_cluster.moocluster.endpoint}"
}

output "gcr_service_account_key" {
  value = "${google_service_account_key.gcr-key.private_key}"
}

output "gke_service_account_key" {
  value = "${google_service_account_key.gke-key.private_key}"
}
